
## 2.0.6 [06-04-2024]

Update IAP version in asset documentation

See merge request itentialopensource/pre-built-automations/cisco-nx-os-iag!54

2024-06-04 21:32:47 +0000

---

## 2.0.5 [04-26-2024]

Changes made at 2024.04.26_11:13AM

See merge request itentialopensource/pre-built-automations/cisco-nx-os-iag!50

2024-04-26 15:16:28 +0000

---

## 2.0.4 [03-16-2024]

* Update readme.json to update vendor in metadata

See merge request itentialopensource/pre-built-automations/cisco-nx-os-iag!48

---

## 2.0.3 [02-20-2024]

* Fixes Pre-Built description

See merge request itentialopensource/pre-built-automations/cisco-nx-os-upgrade!46

---

## 2.0.2 [02-20-2024]

* Fixes Pre-Built description

See merge request itentialopensource/pre-built-automations/cisco-nx-os-upgrade!46

---

## 2.0.1 [02-20-2024]

* Fixes compliance errors

See merge request itentialopensource/pre-built-automations/cisco-nx-os-upgrade!44

---

## 2.0.0 [02-20-2024]

* Migrate new Cisco - NX-OS - IAG Pre-Built

See merge request itentialopensource/pre-built-automations/cisco-nx-os-upgrade!42

---

## 1.0.6 [05-17-2023]

* Update naming convention

See merge request itentialopensource/pre-built-automations/cisco-nx-os-upgrade!38

---

## 1.0.5 [04-27-2023]

* Update IAPDependencies

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!36

---

## 1.0.4 [04-27-2023]

* Update README and Workflow

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!33

---

## 1.0.3 [04-12-2023]

* Updated README to specify IAP 2022.1 dependency.

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!31

---

## 1.0.2 [04-12-2023]

* Updated README to indicate 2022.1 IAP dependency.

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!28

---

## 1.0.1 [04-11-2023]

* Patch/resolve merge conflict with release/2022.1

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!27

---

## 1.0.0 [04-10-2023]

* Refactor work to use Command Template Runner workflow, provide support for IAG and NSO, updates README, and adds automated testing.

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!24

---

## 0.2.5 [02-14-2022]

* Update package.json

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!20

---

## 0.2.4 [02-14-2022]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!19

---

## 0.2.3 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!18

---

## 0.2.2 [03-04-2021]

* patch/DSUP-886-master

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!15

---

## 0.2.1 [09-10-2020]

* Update bundles/transformations/device-firmware-zeroTouch.json,...

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!11

---

## 0.2.0 [06-18-2020]

* [minor/LB-404] Add './' to img path

See merge request itentialopensource/pre-built-automations/cisco-nx-upgrade!8

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab name

See merge request itentialopensource/pre-built-automations/Cisco-NX-Upgrade!7

---\n\n\n\n\n\n\n\n\n\n\n\n\n\n
