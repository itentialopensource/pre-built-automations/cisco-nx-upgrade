# Cisco - NX-OS - IAG

## Overview

This Pre-Built Automation bundle contains several example use cases that are applicable when the Itential Automation Platform is integrated with Cisco NX-OS using the device CLI. Because every environment is different, these use cases are fully functioning examples that can be easily modified to operate in your specific environment. These workflows have been written with modularity in mind to make them easy to understand and simple to modify to suit your needs.


## Workflows


<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href='https://gitlab.com/itentialopensource/pre-built-automations/cisco-nx-os-iag/-/blob/master/documentation/Software Upgrade - NX-OS.md' target='_blank'>Software Upgrade - NX-OS</a></td>
      <td>This example use case automates the procedure of upgrading the software on a Cisco NX-OS network device.</td>
    </tr>
  </tbody>
</table>


## External Dependencies

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Itential Automation Gateway (IAG)</td>
      <td>^3.227.0+2023.1.33</td>
      <td></td>
    </tr>
  </tbody>
</table>

## Adapters

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Version</th>
      <th>Configuration Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>adapter-automation_gateway</td>
      <td>4.29.0-2023.1.12.0</td>
      <td></td>
    </tr>
  </tbody>
</table>