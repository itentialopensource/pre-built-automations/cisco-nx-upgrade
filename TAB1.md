# Overview 

This Pre-Built Automation bundle contains several example use cases that are applicable when the Itential Automation Platform is integrated with Cisco NX-OS using the device CLI. Because every environment is different, these use cases are fully functioning examples that can be easily modified to operate in your specific environment. These workflows have been written with modularity in mind to make them easy to understand and simple to modify to suit your needs.


## Workflows


<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Software Upgrade - NX-OS</td>
      <td>This example use case automates the procedure of upgrading the software on a Cisco NX-OS network device.</td>
    </tr>
  </tbody>
</table>

For further technical details on how to install and use this Workflow Project, please click the Technical Documentation tab. 
