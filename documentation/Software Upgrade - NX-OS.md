# Software Upgrade - NX-OS

## Table of Contents

- [Software Upgrade - NX-OS](#software-upgrade---nx-os)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Getting Started](#getting-started)
    - [Supported IAP Versions](#supported-iap-versions)
    - [External Dependencies](#external-dependencies)
    - [Adapters](#adapters)
    - [How to Install](#how-to-install)
    - [Testing](#testing)
  - [Using this Workflow Project](#using-this-workflow-project)
    - [Entry Point IAP Component](#entry-point-iap-component)
    - [Inputs](#inputs)
    - [Outputs](#outputs)
    - [Query Output](#query-output)
    - [Example Inputs and Outputs](#example-inputs-and-outputs)
    - [API Links](#api-links)
  - [Support](#support)

## Overview

This example use case automates the procedure of upgrading the software on a Cisco NX-OS network device.

Capabilities include:
- Verify current software version and check if the image file used for upgrade is on the device
- Run pre-check commands
- Execute the installation of the new software
- Perform device connection test after software installation
- Verify the new NX-OS image is now active on the device after reload
- Run post-check commands
- Optionally show the difference between the pre-check and post-check results





## Getting Started

### Supported IAP Versions

Itential Workflow Projects are built and tested on particular versions of IAP. In addition, Workflow Projects are often dependent on external systems and as such, these Workflow Projects will have dependencies on these other systems. This version of **Software Upgrade - NX-OS** has been tested with:


- IAP **2023.2**



### External Dependencies

This version of **Software Upgrade - NX-OS** has been tested with:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Itential Automation Gateway (IAG)</td>
      <td>^3.227.0+2023.1.33</td>
      <td></td>
    </tr>
  </tbody>
</table>




### Adapters

This version of **Software Upgrade - NX-OS** has been tested with:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Version</th>
      <th>Configuration Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>adapter-automation_gateway</td>
      <td>4.29.0-2023.1.12.0</td>
      <td></td>
    </tr>
  </tbody>
</table>



### How to Install

To install the Workflow Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Workflow Project.
- Import the Workflow Project in [Admin Essentials](https://docs.itential.com/docs/importing-a-prebuilt-4).

### Testing

While Itential tests this Workflow Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Workflow Project into a development/testing environment in which you can test the Workflow Project.

## Using this Workflow Project


### Entry Point IAP Component

The primary IAP component to run **Software Upgrade - NX-OS** is listed below:

<table>
  <thead>
    <tr>
      <th>IAP Component Name</th>
      <th>IAP Component Type</th>
    </tr>
  </thead>
  <tbody>
      <td>Software Upgrade - NX-OS</td>
      <td>Workflow</td>
    </tr>
  </tbody>
</table>

### Inputs

The following table lists the inputs for **Software Upgrade - NX-OS**:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
      <th>Example Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>deviceName</td>
      <td>string</td>
      <td>yes</td>
      <td>Name of the device in IAG to upgrade</td>
      <td><pre lang="json">CiscoNXOS</pre></td>
    </tr>    <tr>
      <td>suppressFailureMessage</td>
      <td>boolean</td>
      <td>yes</td>
      <td>Whether or not to suppress all manual tasks on failure event occurring</td>
      <td><pre lang="json">false</pre></td>
    </tr>    <tr>
      <td>suppressSuccessMessage</td>
      <td>boolean</td>
      <td>yes</td>
      <td>Whether or not to suppress all manual tasks on success event occurring</td>
      <td><pre lang="json">false</pre></td>
    </tr>    <tr>
      <td>version</td>
      <td>string</td>
      <td>yes</td>
      <td>Firmware image used to upgrade</td>
      <td><pre lang="json">nxos.9.2.2.bin</pre></td>
    </tr>    <tr>
      <td>flashMemory</td>
      <td>string</td>
      <td>yes</td>
      <td>Onboard flash memory storage</td>
      <td><pre lang="json">bootflash:</pre></td>
    </tr>
  </tbody>
</table>



### Outputs

There are no outputs for **Software Upgrade - NX-OS**.


### Query Output

There are no query output examples for **Software Upgrade - NX-OS**.




### Example Inputs and Outputs

  
#### Example 1

    
Input:
<pre>{
  "version": "nxos.9.2.2.bin",
  "flashMemory": "bootflash:",
  "suppressSuccessMessage": true,
  "suppressFailureMessage": true,
  "deviceName": "CiscoNXOS"
} </pre>

    
    
Output:
<pre>{} </pre>

    
  


### API Links


No API Links provided.


## Support

Please use your Itential Customer Success account if you need support when using **Software Upgrade - NX-OS**.